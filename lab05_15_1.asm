global _main

extern _printf
extern _scanf

section .data
    input_message: db "Enter a, b, c, x:", 10, 0
    scanf_format_str: db "%d %d %d %d", 0
    result_printf_str: db "Result: f = %d", 10, 0
    incorrect_data_message: db "Incorrect input", 10, 0
    division_by_zero_message: db "Division by zero", 10, 0

    a: dd 0
    b: dd 0
    c: dd 0
    x: dd 0

section .text
_main:
    push input_message
    call _printf
    add esp, 4

    push x
    push c
    push b
    push a
    push scanf_format_str
    call _scanf
    add esp, 4 * 5

    ; scanf в eax возвращает кол-во успешно заполненных аргументов, таким
    ; образом мы проверяем успешность ввода
    cmp eax, 4
    jne exit_with_error

    mov eax, [a]
    mov ecx, [c]
    mov edi, [x]
    cmp edi, 0
    jl .third_case
    je .second_case
.first_case:
    cmp eax, 0
    jge .third_case

    mov ebx, [b]
    imul ebx, ebx ; ebx ^= 2; ebx == b^2
    imul ebx, edi ; ebx *= x; ebx == x*b^2

    imul eax, eax ; eax ^= 2; eax == a^2
    imul edi, edi ; edi ^= 2; edi == x^2
    imul eax, edi ; eax *= edi; eax == a^2 * x ^ 2

    add eax, ebx
    
    jmp .display_result
.second_case:
    cmp ecx, 0
    jle .third_case

    ; т. к. x = 0, формула тупо сокращается до a / x;
    ; деление на ноль (x - c в знаменателе, x = 0, c > 0)
    ; вообще невозможно и проверка
    ; нафиг не нужна. Вычисление формулы, тем не менее,
    ; написано тупо по заданию

    mov ebx, edi ; ebx == x
    sub ebx, ecx ; ebx -= c

    cdq ; idiv использует регистры edx:eax для делимого. Наше делимое из 32 бит
        ; (eax) нужно расширить до 64 бит (edx:eax). В случае, если число в
        ; eax отрицательное, обнулением edx задачу решить не получится и
        ; нужно использовать cdq (инструкция знакорасширения eax -> edx:eax)
    idiv ebx ; edx:eax / ebx; eax == a / (x - c)

    neg eax ; eax *= -1
    add eax, edi ; += x

    jmp .display_result
.third_case:
    test ecx, ecx ; Проверка возможности деления на ноль
    jz exit_with_div_by_zero_error

    mov eax, edi ; eax == x
    cdq ; на случай отрицательного c, вместо xor edx, edx; см. выше
    idiv ecx ; eax /= c

    inc eax
.display_result:
    push eax
    push result_printf_str
    call _printf
    add esp, 4 * 2

    xor eax, eax
    ret
exit_with_error:
    push incorrect_data_message
    call _printf
    add esp, 4

    mov eax, 1
    ret
exit_with_div_by_zero_error:
    push division_by_zero_message
    call _printf
    add esp, 4

    mov eax, 2
    ret
