global _main

extern _printf

section .data
    source_text: db "look for the stars look for onlookers lolook "
                 db "prolong lo loo look looks   lo look look to come here "
                 db "look"
    source_text_end: db 0
    source_text_length: dd $ - source_text - 1

    search_token: db "look ", 0
    search_token_length: dd $ - search_token - 1

    replacement_token: db "stop ", 0
    replacement_token_length: dd $ - replacement_token - 1

    source_text_message: db "source text: '%s'", 10, 0
    modified_text_message: db "modified text: '%s'", 10, 0
section .text
_main:
    ; "Слово" в терминологии подобных заданий - подстрока, ограниченная
    ; пробелами (или началом, концом текста)

    push source_text
    push source_text_message
    call _printf
    add esp, 4 * 2
    
    mov edi, source_text
    mov dword [source_text_end], ' ' ; подстрока может быть ограничена концом
                                     ; текста. нуль в конце нам в любом случае
                                     ; по алгоритму не нужен, временно заменяем
                                     ; на пробел

    mov al, [source_text] ; В дальнейшем будем искать пробелы для определения
                          ; начала искомой подстроки. Однако возможно, что в
                          ; начале есть подстрока, ограниченная началом текста.
                          ; Обрабатываем этот случай сравнением первых символов
    mov bl, [search_token]

    cmp al, bl
    je .compare_substrings

    inc edi ; В дальнейшем перед каждым j** .continue_search по логике следует
            ; написать dec edi, но в таком случае изменятся флаги и может быть
            ; некорректный jmp. Помещаем dec edi прямо после .continue_search;
            ; dec edi необходим, т. к. имеет место быть лишнее смещение edi + 1
            ; через команды cmpsb, movsb
.continue_search:
    dec edi

    mov ecx, source_text_end
    sub ecx, edi ; высчитываем кол-во символов до конца строки

    cmp ecx, 0
    jle .display_modified_message

    mov eax, ' '

    repne scasb ; Повторять, пока не закончится (обнулится) ecx или не найдётся
                ; eax в edi - ищем пробел. Суффикс b обозначает byte - 
                ; работаем побайтово
    jne .display_modified_message ; ecx закончился
.compare_substrings:
    mov ecx, [search_token_length]
    lea esi, [search_token]

    repe cmpsb ; сравниваем подстроки
    jne .continue_search ; подстроки не сошлись

    sub edi, [search_token_length]

    mov ecx, [replacement_token_length]
    mov esi, replacement_token
    rep movsb ; esi -> edi ; заменяем подстроки

    jmp .continue_search
.display_modified_message:
    mov dword [source_text_end], 0 ; заменяем пробел на 0

    push source_text
    push modified_text_message
    call _printf
    add esp, 4 * 2
    
    xor eax, eax
    ret
