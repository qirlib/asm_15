global _main

extern _printf
extern _scanf

extern _calculateFormula
extern _getNumberFromParts

extern _isDivisible
extern _findNeededNumbers

extern _calculateGeometricMean
extern _find3max

section .data
    lf_scanf: db "%lf", 0
    u3_scanf: db "%u %u %u", 0
    u2_scanf: db "%u %u", 0

    d_printf: db "%d ", 0

    lab1_1_splash: db "** Lab01/1:", 10, "* Enter a: ", 0
    lab1_1_info: db "* Result of calculateFormula = %lf", 10, 0

    lab1_2_splash: db "** Lab01/2:", 10, "* Enter three one-digit numbers: ", 0
    lab1_2_info: db "* Result of getNumberFromParts = %d", 10, 0
    
    lab2_1_splash: db "** Lab02/1:", 10, "* Enter two-digit number and m: ", 0
    lab2_1_info: db "* Result of isDivisible = %d", 10, 0

    lab2_2_splash: db "** Lab02/2:", 10, 0

    lab3_1_splash: db "** Lab03/1:", 10
                   db "* Result of calculateGeometricMean = %G", 10
                   db "* (array used): ", 0
    lab3_1_splash_end: db 10, 0

    lab3_2_splash: db "** Lab03/2 (same array used):", 10, 0

    array: dd 5, 6, 15, 8, 9, 100, 5, 5, 4, 4
           dd 14, 101, 8, 9, 11, 12, 9, 5, 9, 9
    array_length: dd ($ - array) / 4

section .text
_main:
    ; lab01/1

    push lab1_1_splash
    call _printf

    sub esp, 4 ; выделить место на стеке для хранения временного значения

    push esp
    push lf_scanf
    call _scanf
    add esp, 4 * 2

    call _calculateFormula

    fstp qword [esp]
    push lab1_1_info
    call _printf
    add esp, 4 + 8

    ; lab01/2

    push lab1_2_splash
    call _printf
    add esp, 4

    sub esp, 4 * 3
    lea eax, [esp + 8]
    push eax
    lea eax, [esp + 8]
    push eax
    lea eax, [esp + 8]
    push eax
    push u3_scanf
    call _scanf
    add esp, 4 * 4

    call _getNumberFromParts
    add esp, 4 * 3

    push eax
    push lab1_2_info
    call _printf
    add esp, 4 * 2

    ; lab02/1

    push lab2_1_splash
    call _printf
    add esp, 4

    sub esp, 4 * 2
    lea eax, [esp + 4]
    push eax
    lea eax, [esp + 4]
    push eax
    push u2_scanf
    call _scanf
    add esp, 4 * 3

    call _isDivisible
    add esp, 4 * 2

    push eax
    push lab2_1_info
    call _printf
    add esp, 4 * 2

    ; lab02/2

    push lab2_2_splash
    call _printf
    add esp, 4

    call _findNeededNumbers

    ; lab03/1

    push dword [array_length]
    push array
    call _calculateGeometricMean
    fstp qword [esp]
    push lab3_1_splash
    call _printf
    add esp, 4 + 8

    xor esi, esi
    mov edi, [array_length]

    push 0
    push d_printf
.display_array_loop:
    mov eax, [array + 4 * esi]
    mov [esp + 4], eax
    call _printf
    inc esi
    cmp esi, edi
    jb .display_array_loop

    add esp, 4 * 2

    push lab3_1_splash_end
    call _printf
    add esp, 4

    ; lab03/2

    push lab3_2_splash
    call _printf
    add esp, 4

    push dword [array_length]
    push array
    call _find3max
    add esp, 4 * 2

    xor eax, eax
    ret
