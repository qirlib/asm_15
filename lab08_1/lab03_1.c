#include <math.h>

double calculateGeometricMean(int* array, int N)
{
    double p = 1;
    
    for(int i = 0; i < N; i++)
        p *= array[i];
    
    return pow(p, 1.0 / N);
}
