int isDivisible(int number, int m)
{   
    return (number / 10 + number % 10) % m == 0;
}
