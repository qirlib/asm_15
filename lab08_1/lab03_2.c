#include <stdio.h>
#include <stdlib.h>

int cmpfunc(const void *a, const void *b) {
    return (*(int *) a - *(int *) b);
}

void find3max(int* array, int n)
{
    qsort(array, n, sizeof(int), cmpfunc);

    printf("Max elements: %d, %d, %d\n", array[n - 1], array[n - 2], array[n - 3]);
}