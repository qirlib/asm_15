#include <stdio.h>

void findNeededNumbers()
{
    const int n = 14, d = 19, s = 171;
    
    for(int i = 0; i < n; ++i)
        printf("%d\n", s + i * d);
}