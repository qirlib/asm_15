global _stoogeSort

section .text
stooge_sort: ; edi = i, esi = j
    mov eax, [edx + 4 * esi]
    mov ebx, [edx + 4 * edi]

    cmp eax, ebx
    jge .skip_xchg
    mov [edx + 4 * esi], ebx
    mov [edx + 4 * edi], eax
.skip_xchg:
    mov eax, esi
    sub eax, edi ; eax = j - i
    cmp eax, 1
    jle .skip_to_end

    inc eax

    mov ecx, 3
    push edx
    xor edx, edx ; в eax всегда будет положительное значение, cdq не нужно
    idiv ecx
    pop edx

    push eax

    push esi
    sub esi, [esp + 4] ; обращение к eax в стеке
    call stooge_sort
    pop esi

    push edi
    add edi, [esp + 4]
    call stooge_sort
    pop edi

    push esi
    sub esi, [esp + 4]
    call stooge_sort
    pop esi
    
    add esp, 4
.skip_to_end:
    ret
_stoogeSort:
    push ebp
    mov ebp, esp

    push ebx
    push esi
    push edi

    mov edx, [ebp + 8]

    mov edi, 0
    mov esi, [ebp + 12]
    dec esi

    call stooge_sort

    pop edi
    pop esi
    pop ebx

    pop ebp
    ret
