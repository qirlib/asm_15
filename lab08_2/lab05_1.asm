global _calculateF

section .text
_calculateF:
    push ebp
    mov ebp, esp

    push ebx
    push edi

    mov eax, [ebp + 8] ; a
    mov ecx, [ebp + 8 + 8] ; c
    mov edi, [ebp + 8 + 12] ; x
    cmp edi, 0
    jl .third_case
    je .second_case
.first_case:
    cmp eax, 0
    jge .third_case

    mov ebx, [ebp + 8 + 4] ; b
    imul ebx, ebx ; ebx ^= 2; ebx == b^2
    imul ebx, edi ; ebx *= x; ebx == x*b^2

    imul eax, eax ; eax ^= 2; eax == a^2
    imul edi, edi ; edi ^= 2; edi == x^2
    imul eax, edi ; eax *= edi; eax == a^2 * x ^ 2

    add eax, ebx
    
    jmp .end
.second_case:
    cmp ecx, 0
    jle .third_case

    ; т. к. x = 0, формула тупо сокращается до a / x;
    ; деление на ноль (x - c в знаменателе, x = 0, c > 0)
    ; вообще невозможно и проверка
    ; нафиг не нужна. Вычисление формулы, тем не менее,
    ; написано тупо по заданию

    mov ebx, edi ; ebx == x
    sub ebx, ecx ; ebx -= c

    cdq ; idiv использует регистры edx:eax для делимого. Наше делимое из 32 бит
        ; (eax) нужно расширить до 64 бит (edx:eax). В случае, если число в
        ; eax отрицательное, обнулением edx задачу решить не получится и
        ; нужно использовать cdq (инструкция знакорасширения eax -> edx:eax)
    idiv ebx ; edx:eax / ebx; eax == a / (x - c)

    neg eax ; eax *= -1
    add eax, edi ; += x

    jmp .end
.third_case:
    mov eax, edi ; eax == x
    cdq ; на случай отрицательного c, вместо xor edx, edx; см. выше
    idiv ecx ; eax /= c

    inc eax
.end:
    pop edi
    pop ebx
    
    pop ebp
    ret
