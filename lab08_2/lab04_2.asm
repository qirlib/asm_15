; Используя простые числа 5, 4, 5, 1 и простые математические действия
; умножение, сложение, вычитание и деление получите число 24.
; 5 * 4 + 5 - 1 = 24

global _getNumber

section .data
    number_one dq 5.0
    number_two dq 4.0
    number_three dq 5.0
    number_four dq 1.0

section .text
_getNumber:
    push ebp
    mov ebp, esp

    fld qword [number_one]      ; Загрузка операнда в вершину стека
    fld qword [number_two]      ; Загрузка операнда в вершину стека
    fmulp                       ; Умножение 5 * 4
    fld qword [number_three]    ; Загрузка операнда в вершину стека
    faddp                        ; Сложение 20 + 5
    fld qword [number_four]     ; Загрузка операнда в вершину стека
    fsubp                        ; Вычитание 25 - 1

    pop ebp
    ret                         ; выход
