; Найти значение выражения: 
; Z = (sqrt((m) ? sqrt(n)) / (m + n)
; при m = 25 и n = 16 Z = (5 - 4) / 41 = 0.02

global _calculateFormula

segment .text
_calculateFormula :
    push ebp
    mov ebp, esp

    fld qword [ebp + 8]     ; Загрузка операнда в вершину стека, m
    fsqrt                   ; sqrt((m)
    fld qword [ebp + 16]    ; Загрузка операнда в вершину стека, n
    fsqrt                   ; sqrt(n)
    fsubp                   ; sqrt(m) - sqrt(n) в ST(1), затем удаляем элемент

    fld qword [ebp + 8]     ; Загрузка операнда в вершину стека
    fld qword [ebp + 16]    ; Загрузка операнда в вершину стека
    faddp                   ; (m + n) в ST(1), затем удаляем элемент со стека
    fdivp                   ; Деление ST(1) на ST(0), затем удаляем элемент
    
    pop ebp
    ret                     ; Возвращаем значение в fp0, выход
