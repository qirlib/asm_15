#include <stdio.h>
#include <stdlib.h>

extern double calculateFormula(double m, double n);
extern double getNumber();

extern int calculateF(int a, int b, int c, int x);
extern void displayNeededNumbers(int n, int k);

extern void stoogeSort(int* array, int size);
extern void replaceLookStop(char* str);

int main()
{
    double m, n;
    printf("** Lab04/1:\n* Enter m and n: ");
    scanf("%lf %lf", &m, &n);
    printf("* Result of calculateFormula = %lf\n", calculateFormula(m, n));
    
    printf("** Lab04/2:\n* Result of getNumber = %lf\n", getNumber());
    
    int a, b, c, x;
    printf("** Lab05/1:\n* Enter a, b, c, x: ");
    scanf("%d %d %d %d", &a, &b, &c, &x);
    printf("* Result of calculateF = %d\n", calculateF(a, b, c, x));
    
    int n2, k;
    printf("** Lab05/2:\n* Enter n and k: ");
    scanf("%d %d", &n2, &k);
    displayNeededNumbers(n2, k);
    
    printf("** Lab06/1:\n* Enter array length: ");
    scanf("%d", &n2);
    
    if(n2 != 0)
    {
        int* array = malloc(sizeof(int) * n2);
        if(!array)
            printf("* Failed to allocate memory.\n");
        else
        {
            printf("* Enter array: ");
            for(k = 0; k < n2; k++)
                if(scanf("%d", &array[k]) != 1)
                {
                    printf("* Invalid input.\n");
                    break;
                }

            if(k == n2)
            {
                stoogeSort(array, n2);

                printf("* Array after stoogeSort: ");

                for(k = 0; k < n2; k++)
                    printf("%d ", array[k]);

                putchar('\n');
            }

            free(array);
        }
    }
    else
        printf("* Invalid array length.\n");
    
    while(getchar() != '\n'); // следующая команда чтения (fgets) ищет
                              // символ новой строки, а scanf последний
                              // введённый символ новой строки не трогает.
                              // Избавляемся от него
    
    printf("** Lab06/2:\n* Enter string: ");
    // look for the stars look for onlookers lolook prolong lo loo look looks   lo look look to come here look
    char str[1024];
    fgets(str, 1024, stdin); // считать строку из входного потока до 1024
    
    // fgets также копирует символ новой строки, избавляемся от него
    for(char* c = str; *c != '\0'; c++)
        if(*c == '\n')
        {
            *c = '\0';
            break;
        }
    
    replaceLookStop(str);
    printf("* String after replaceLookStop: '%s'", str);
    
    return 0;
}

