global _replaceLookStop

section .data
    search_token: db "look ", 0
    search_token_length: dd $ - search_token - 1

    replacement_token: db "stop ", 0
    replacement_token_length: dd $ - replacement_token - 1

section .text
_replaceLookStop:
    ; "Слово" в терминологии подобных заданий - подстрока, ограниченная
    ; пробелами (или началом, концом текста)

    push ebp
    mov ebp, esp

    push ebx
    push esi
    push edi

    mov edi, [ebp + 8]
    xor eax, eax
    mov ecx, -1
    repne scasb

    dec edi

    sub edi, [ebp + 8]
    mov ebx, edi

    mov edi, [ebp + 8]
    mov dword [edi + ebx], ' ' ; подстрока может быть ограничена концом
                               ; текста. нуль в конце нам в любом случае
                               ; по алгоритму не нужен, временно заменяем
                               ; на пробел

    mov al, [edi]
    mov cl, [search_token] ; В дальнейшем будем искать пробелы для определения
                           ; начала искомой подстроки. Однако возможно, что в
                           ; начале есть подстрока, ограниченная началом текста.
                           ; Обрабатываем этот случай сравнением первых символов

    cmp al, cl
    je .compare_substrings

    inc edi
.continue_search:
    dec edi

    mov ecx, [ebp + 8]
    lea ecx, [ecx + ebx]
    sub ecx, edi ; высчитываем кол-во символов до конца строки

    cmp ecx, 0
    jle .display_modified_message

    mov eax, ' '

    repne scasb ; Повторять, пока не закончится (обнулится) ecx или не найдётся
                ; eax в edi - ищем пробел. Суффикс b обозначает byte - 
                ; работаем побайтово
    jne .display_modified_message
.compare_substrings:
    mov ecx, [search_token_length]
    lea esi, [search_token]

    repe cmpsb ; сравниваем подстроки
    jne .continue_search

    sub edi, [search_token_length]

    mov ecx, [replacement_token_length]
    mov esi, replacement_token
    rep movsb ; esi -> edi ; заменяем подстроки

    jmp .continue_search
.display_modified_message:
    mov edi, [ebp + 8]
    mov dword [edi + ebx], 0 ; заменяем пробел на 0
    
    pop edi
    pop esi
    pop ebx

    pop ebp
    ret
