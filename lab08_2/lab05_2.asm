global _displayNeededNumbers

extern _printf

section .data
    printf_format: db "%d", 10, 0
    numbers_message: db "numbers:", 10, 0
    error_message: db "invalid input", 10, 0

section .text
_displayNeededNumbers:
    push ebp
    mov ebp, esp

    push ebx
    push esi
    push edi

    mov edi, [ebp + 8] ; n
    cmp edi, 0 ; n должно быть > 0
    jle .exit_with_error_message

    push numbers_message
    call _printf
    add esp, 4

    mov ecx, edi

    mov eax, 1
.power_loop:
    imul eax, 10
    loop .power_loop

    mov esi, eax

    xor edx, edx
    mov ebx, 10
    idiv ebx

    cmp edi, 1 ; n == 1?
    jne .non_1
    dec eax
.non_1:
    mov edi, eax

.main_loop:
    mov eax, edi
    mov ebx, 10
    xor ecx, ecx
.inner_loop:
    xor edx, edx
    idiv ebx
    add ecx, edx
    test eax, eax
    jnz .inner_loop

    cmp ecx, [ebp + 8 + 4] ; k
    jne .skip_output

    push edi
    push printf_format
    call _printf
    add esp, 4 * 2

.skip_output:
    inc edi
    cmp edi, esi
    jne .main_loop

.end:
    pop edi
    pop esi
    pop ebx

    pop ebp
    ret
.exit_with_error_message:
    push error_message
    call _printf
    add esp, 4
    jmp .end
