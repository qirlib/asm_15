global _main

extern _printf

section .data
    source_array: dd 10, 1, 4, 34, 20, -5, -6, 0, 534, 0, -5, 20
    sa_length: dd ($ - source_array) / 4

    source_array_message: db "source array: ", 0
    sorted_array_message: db 10, "sorted array: ", 0
    newline_message: db 10, 0
    num_printf_str: dd "%d ", 0

section .text
stooge_sort: ; edi = i, esi = j
    mov eax, [source_array + 4 * esi]
    mov ebx, [source_array + 4 * edi]

    cmp eax, ebx
    jge .skip_xchg
    mov [source_array + 4 * esi], ebx
    mov [source_array + 4 * edi], eax
.skip_xchg:
    mov eax, esi
    sub eax, edi ; eax = j - i
    cmp eax, 1
    jle .skip_to_end

    inc eax

    mov ecx, 3
    xor edx, edx ; в eax всегда будет положительное значение, cdq не нужно
    idiv ecx

    push eax

    push esi
    sub esi, [esp + 4] ; обращение к eax в стеке
    call stooge_sort
    pop esi

    push edi
    add edi, [esp + 4]
    call stooge_sort
    pop edi

    push esi
    sub esi, [esp + 4]
    call stooge_sort
    pop esi
    
    add esp, 4
.skip_to_end:
    ret
display_array:
    xor edi, edi
    mov esi, [sa_length]

    push 0
    push num_printf_str
.da_loop:
    mov eax, [source_array + edi * 4]
    mov [esp + 4], eax ; меняем аргументы для _printf прямо в стеке
    call _printf

    inc edi
    cmp edi, esi
    jne .da_loop

    add esp, 4 * 2

    ret
_main:
    push source_array_message
    call _printf
    add esp, 4
    call display_array

    mov edi, 0
    mov esi, [sa_length]
    dec esi

    call stooge_sort

    push sorted_array_message
    call _printf
    add esp, 4
    call display_array

    push newline_message
    call _printf
    add esp, 4

    xor eax, eax
    ret
